const Product = require("../models/Products");


module.exports.addProduct = (reqBody) => {

	let newProduct = new Product ({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	return newProduct.save().then(product => true)
	.catch(err => false)
};


module.exports.getAllProducts = () => {
	return Product.find({}).then(result => result)
	.catch(err => err);

};

module.exports.getAllActive = () => {
	return Product.find({ isActive : true }).then(result => result)
	.catch(err => err);

};


module.exports.getProduct = (productId, reqBody) =>{
	return Product.findById(productId)
	.then((result,error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return result
		}
	})
};


module.exports.updateProduct = (productId, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	return Product.findByIdAndUpdate(productId, updatedProduct)
	.then(product => true)
	.catch(err => err);

};

module.exports.archiveProduct = (reqParams, reqBody) => {
	let archivedCourse = {
		isActive: reqBody.isActive
	};
	return Product.findByIdAndUpdate(reqParams.productId, archivedCourse)
	.then(product => true)
	.catch(err => err)
	if(err){
		console.log(error);
		return false;
	}else{
		return result
	}
};
